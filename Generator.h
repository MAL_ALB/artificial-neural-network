#ifndef Generator_H
#define Generator_H

#include <iostream>
#include <fstream>
#include <limits>
#include "Matrix.h"
using std::cout;
using std::endl;
using std::vector;

struct Rckg { vector<double> input; vector<double> lable; };


class Generator {
public:
	Generator(std::string _input_filename, std::string _lable_filename, int _vectorinput_size, int _vectoroutput_size, int _filesize = 60000);
	void get_data(std::string input_filename, std::string lable_filename);
	Rckg generate(void);


private:
	double* input[1000];
	double* lable[1000];
	int position;
	int filesize;
	std::string filename;
	int vectorinput_size, vectoroutput_size;
	std::string input_filename, lable_filename;
};




#endif // !Generator_H