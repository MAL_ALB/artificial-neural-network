#ifndef ANN_H
#define ANN_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <cmath>
#include "Matrix.h"
#include "Generator.h"
using std::vector;
using std::ostream;
using std::istream;
using std::cout;
using std::endl;
using std::pow;




class ANN {
public:
	ANN(vector<int> v, double (*activation)(double), double (*del_activation)(double));
	ANN(vector<Matrix<double>> Weights, vector<Matrix<double>> biases, double (*activation)(double), double (*del_activation)(double));
	ANN(std::string filename, double (*activation)(double), double (*del_activation)(double));
	Matrix<double> Propagation(vector<double> input);
	void Backpropagation(Generator& G, double learnrate, int batchsize);
	double Acuracy_test(Generator& G, int testsamplesize = 1000);
	vector<double> Checkout_learnrate(Generator& G, int number_of_train_samples, int learning_steps, float learnrate = 1);
	friend ostream& operator<<(ostream& os, const ANN& A);
	//friend std::ifstream& operator>>(std::ifstream& in, const ANN& A);
	vector<Matrix<double>> get_weights();
	int get_batchsize() const;
	void save(std::string savename);

private:
	vector<Matrix<double>> H;
	vector<Matrix<double>> bias;
	double (*activation)(double);
	double (*del_activation)(double);
};

#endif // !ANN_H

