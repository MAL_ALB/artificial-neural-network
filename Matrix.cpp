#include "Matrix.h"



template<class T>
void Matrix<T>::save(std::string savename) {
	using namespace std;
	ofstream Outdata;
	Outdata.open(savename + ".dat");
	if (!Outdata) {
		cerr << "File coulden't be opened" << endl;
		exit(1);
	}
	Outdata << *this << endl;
	Outdata.close();
}