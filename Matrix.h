#ifndef Matrix_H
#define Matrix_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
using std::vector;
using std::ostream;
using std::istream;
using std::cout;
using std::endl;

template<class T>
class Matrix {
public:
	Matrix() {
		n = 0;
		m = 0;
		entries = vector<T>();
	}
	Matrix(int _n, int _m) {
		n = _n;
		m = _m;
		entries = vector<double>(n * m, 0);
	}
	Matrix(int _n, int _m, vector<T> vec) {
		n = _n;
		m = _m;
		if (!((n * m) == vec.size())) {
			exit(0);
		}
		entries = vec;
	}
	//Operatoren

	Matrix<T> operator*(const Matrix<T>& B) {
		if (!(m == B.n)) {
			exit(1);
		}
		vector<T> C(n * B.m, 0);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < B.m; j++) {
				for (int k = 0; k < m; k++)
					C[j + i * B.m] += entries[k + i * m] * B(k, j);
			}
		}
		return Matrix(n, B.m, C);
	}
	Matrix<T> operator*(const vector<T>& v) {
		return operator*(Matrix<T>(v.size(), 1, v));
	}
	Matrix<T> operator+(const Matrix<T>& B) {
		if (!(m == B.m and n == B.n)) {
			exit(1);
		}
		vector<T> C(n * m, 0);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				C[j + i * m] = entries[j + i * m] + B(i, j);
			}
		}
		return Matrix<T>(n, m, C);
	}
	Matrix<T> operator-(const Matrix& B) {
		if (!(m == B.m and n == B.n)) {
			exit(1);
		}
		vector<T> C(n * m, 0);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				C[j + i * m] = entries[j + i * m] - B(i, j);
			}
		}
		return Matrix<T>(n, m, C);
	}
	Matrix<T> operator-(const vector<T> v) {
		Matrix<T> V(v.size(), 1, v);
		return operator-(V);
	}
	Matrix<T> operator%(const Matrix<T>& B) { //Operator for elementwise multiplication
		if (!(m == B.m and n == B.n)) {
			exit(1);
		}
		vector<T> C(n * m, 0);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				C[j + i * m] = entries[j + i * m] * B(i, j);
			}
		}
		return Matrix<T>(n, m, C);
	}
	Matrix transpose() {
		Matrix<T> B(m, n);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				B(j, i) = operator()(i, j);
			}
		}
		return B;
	}
	friend ostream& operator<<(ostream& os, const Matrix<T>& A) {
		for (int i = 0; i < A.n; i++) {
			for (int j = 0; j < A.m; j++) {
				os << A(i, j) << "  ";
			}
			os << endl;
		}
		return os;
	}
	friend std::ifstream& operator>>(std::ifstream& in, Matrix<T>& A) {
		using namespace std;
		string line;
		int counter = -1;
		int size_ges = 0;
		vector<T> store;
		while (getline(in, line))
		{
			counter++;
			istringstream iss(line);
			T a;
			if (!(iss)) { break; } // error
			while (iss >> a) {
				store.push_back(a);
				size_ges++;
			}

		}
		A.n = size_ges / counter;
		A.m = counter;
		A.entries = store;
		return in;
	}
	int get_n() const {
		return n;
	}
	int get_m() const {
		return m;
	}
	double operator()(int row, int collumn) const {//operator to read out elements
		if (!(0 <= row && row < n && 0 <= collumn && collumn < m)) {
			exit(1);
		}
		return entries[row * m + collumn];
	}
	T& operator()(int row, int collumn) {
		if (!(0 <= row && row < n && 0 <= collumn && collumn < m)) {
			exit(1);
		}
		return entries[row * m + collumn];
	}
	void save(std::string savename);
	friend Matrix<T> operator*(double zahl, const Matrix<T>& A) {
		Matrix<T> B(A.get_n(), A.get_m());
		for (int i = 0; i < A.n; i++) {
			for (int j = 0; j < A.m; j++) {
				B(i, j) = zahl * A(i, j);
			}
		}
		return B;
	}
private:
	int n, m;
	vector<T> entries;
};






#endif /*Matrix_H*/
