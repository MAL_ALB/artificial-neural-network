#include "ANN.h"


//Auxiliary function
Matrix<double> Map(Matrix<double>& A, double (*f)(double)) {
	Matrix<double> B(A.get_n(), A.get_m());
	for (int i = 0; i < A.get_n(); i++) {
		for (int j = 0; j < A.get_m(); j++) {
			B(i, j) = (*f)(A(i, j));
		}
	}
	return B;
}

//Class functions
vector<Matrix<double>> ANN::get_weights() {
	return H;
}
int ANN::get_batchsize() const {
	return 1;
}
double mmax(Matrix<double>& M)
{
	double max = M(0, 0);
	for (int i = 0; i < M.get_n(); i++)
	{
		for (int j = 0; j < M.get_m(); j++)
		{
			max = (max < M(i, j)) ? M(i, j) : max;
		}
	}
	return max;
}
double mmin(Matrix<double>& M)
{
	double min = M(0, 0);
	for (int i = 0; i < M.get_n(); i++)
	{
		for (int j = 0; j < M.get_m(); j++)
		{
			min = (min > M(i, j)) ? M(i, j) : min;
		}
	}
	return min;
}
Matrix<double> normalize(Matrix<double>& M)
{
	Matrix<double> N(M.get_n(), M.get_m());
	double max = mmax(M);
	double min = mmin(M);
	for (int i = 0; i < M.get_n(); i++)
	{
		for (int j = 0; j < M.get_m(); j++)
		{
			N(i, j) = (M(i, j) - min) / (max - min);
		}
	}
	return N;
}
ANN::ANN(vector<int> v, double (*_activation)(double), double (*_del_activation)(double)) :activation(_activation), del_activation(_del_activation) {
	 
	for (std::size_t i = 0; i < v.size() - 1; i++) {
		vector<double> k;
		for (int j = 0; j < v[i + 1] * v[i]; j++) {
			k.push_back(((double)rand() / RAND_MAX) * 2 - 1);
		}
		H.push_back(Matrix<double>(v[i + 1], v[i], k));
	}
	for (std::size_t i = 0; i < v.size() - 1; i++) {
		vector<double> h;
		for (int j = 0; j < v[i + 1]; j++) {
			h.push_back(((double)rand() / RAND_MAX) * 2 - 1);
		}
		bias.push_back(Matrix<double>(v[i + 1], 1, h));
	}
}

ANN::ANN(vector<Matrix<double>> Weights, vector<Matrix<double>> biases, double (*_activation)(double), double (*_del_activation)(double)) : activation(*_activation), del_activation(*_del_activation) {
	H = Weights;
	bias = biases;
}

ANN::ANN(std::string filename, double (*_activation)(double), double (*_del_activation)(double)) : activation(_activation), del_activation(_del_activation) {
	std::string line;
	std::ifstream Filecontent(filename);
	vector<vector<double>> container;                      
	vector<int> m_counter, n_counter;                       
	int counter = -1;
	while (std::getline(Filecontent, line)) {
		vector<double> line_container;
		std::istringstream iss(line);
		double c;
		while (iss >> c) {
			line_container.push_back(c);
			m_counter[counter] += 1;
		}
		if (iss.fail() and !iss.eof()) {
			n_counter.push_back(0);					//Use the fact that file is begining with string to initiate counters
			m_counter.push_back(0);
			line.clear();
			iss.clear();
			std::string dummy;
			iss >> dummy;
			container.push_back(vector<double>());
			if (counter >= 0) {
				n_counter[counter] -= 1;
				m_counter[counter] = m_counter[counter] / (n_counter[counter]);
			}
			counter++;
		}
		else {
			n_counter[counter] += 1;
			container[counter].insert(container[counter].end(), line_container.begin(), line_container.end());
		}
	}
	Filecontent.close();
	vector<Matrix<double>> weights, biases;
	for (std::size_t kl = 0; kl < (container.size() / 2); kl++) {
		weights.push_back(Matrix<double>(n_counter[2 * kl], m_counter[2 * kl], container[2 * kl]));
		biases.push_back(Matrix<double>(n_counter[2 * kl], 1, container[2 * kl + 1]));
	}
	H = weights;
	bias = biases;
}
void ANN::Backpropagation(Generator& G, double learnrate, int batchsize) {
	vector<Matrix<double>> a, z;

	vector<Matrix<double>> delta_b_added, delta_w_added;
	for (std::size_t l = 0; l < H.size(); l++) {
		delta_b_added.push_back(Matrix<double>(bias[l].get_n(), bias[l].get_m()));
		delta_w_added.push_back(Matrix<double>(H[l].get_n(), H[l].get_m()));
	}
	for (int k = 0; k < batchsize; k++) {
		Rckg data = G.generate();
		Matrix<double> container(data.input.size(), 1, data.input);
		for (std::size_t i = 0; i < H.size(); i++) {
			container = (H[i] * container) + bias[i];
			z.push_back(container);
			container = Map(container, (*activation));
			a.push_back(container);
		}

		vector<Matrix<double>> delta(H.size());
		delta[H.size() - 1] = Map(z[H.size() - 1], (*del_activation)) % (a[H.size() - 1] - data.lable);
		for (int i = H.size() - 2; i >= 0; i--) {
			delta[i] = Map(z[i], (*del_activation)) % (H[i + 1].transpose() * (delta[i + 1]));
		}
		vector<Matrix<double>> delta_w;
		delta_w.push_back(delta[0] * Matrix<double>(1, data.input.size(), data.input));
		for (std::size_t i = 1; i < H.size(); i++) {
			delta_w.push_back(delta[i] * a[i - 1].transpose());
		}

		for (std::size_t i = 0; i < H.size(); i++) {								//Aufsammeln der Gewichte
			delta_w_added[i] = delta_w_added[i] - learnrate * delta_w[i];
			delta_b_added[i] = delta_b_added[i] - learnrate * delta[i];
		}
	}
	for (std::size_t i = 0; i < H.size(); i++) {
		H[i] = H[i] + (1 / batchsize) * delta_w_added[i];
		bias[i] = bias[i] + (1 / batchsize) * delta_b_added[i];
	}

}

Matrix<double> ANN::Propagation(vector<double> input) {
	Matrix<double> container(input.size(), 1, input);
	for (std::size_t i = 0; i < H.size(); i++) {
		container = (H[i] * container) + bias[i];
		container = Map(container, (*activation));
	}
	return normalize(container);
}
ostream& operator<<(ostream& os, const ANN& A) {
	for (std::size_t i = 0; i < A.H.size(); i++) {
		os << "Weights des " << i << "ten Layers: " << endl << A.H[i] << endl;
		os << "Biases des " << i << "ten Layers: " << endl << A.bias[i] << endl;
		os << endl;
	}
	os << endl;
	os << endl;
	return os;
}
void ANN::save(std::string savename) {
	using namespace std;
	ofstream Outdata;
	Outdata.open(savename + ".dat");
	if (!Outdata) {
		cerr << "File coulden't be opened" << endl;
		exit(1);
	}
	Outdata << *this << endl;
	Outdata.close();
}
double ANN::Acuracy_test(Generator& G, int testsamplesize) {
	double correct = 0, not_correct = 0;
	for (int j = 0; j <= testsamplesize; j++) {
		Rckg data = G.generate();
		Matrix<double> k = Propagation(data.input);
		if (k(std::distance(data.lable.begin(), std::max_element(data.lable.begin(), data.lable.end())), 0) == 1) {
			correct += 1;
		}
		else {
			not_correct += 1;
		}

	}
	return correct / (correct + not_correct);
}

vector<double> ANN::Checkout_learnrate(Generator& G, int number_of_train_samples, int learning_steps, float learnrate) {
	vector<double> acuracy;
	for (int i = 0; i < number_of_train_samples / learning_steps; i++) {
		for (int j = 0; j <= learning_steps; j++) {
			Backpropagation(G, 0.25 * (1.05 - i / ((double)number_of_train_samples / learning_steps) * learnrate), 1);
		}
		double Acc = Acuracy_test(G, 1000);
		acuracy.push_back(Acc);
	}
	return acuracy;
}
