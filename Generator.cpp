#include "Generator.h"

std::fstream& gotoLine(std::fstream& file, unsigned int num) {
	file.seekg(std::ios::beg);
	for (int i = 0; i < num - 1; ++i) {
		file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
	return file;
}

Generator::Generator(std::string _input_filename, std::string _lable_filename, int _vectorinput_size, int _vectoroutput_size, int _filesize) : position(1), filesize(_filesize), lable_filename(_lable_filename), input_filename(_input_filename), vectorinput_size(_vectorinput_size), vectoroutput_size(_vectoroutput_size) {
	for (int i = 0; i < 1000; i++) {
		input[i] = new double[vectorinput_size];
		lable[i] = new double[vectoroutput_size];
	}
	get_data(input_filename, lable_filename);
}

void Generator::get_data(std::string input_filename, std::string lable_filename) {
	std::fstream File_input(input_filename);
	if (File_input.is_open()) {
		gotoLine(File_input, position);
		std::string line;
		int counter = 0;

		while (std::getline(File_input, line) and counter < 1000) {
			std::istringstream iss(line);
			double a;
			int counter_2 = 0;
			while (iss >> a) {
				input[counter][counter_2] = a;
				counter_2++;
			}
			counter++;
		}
		File_input.close();
	}
	std::fstream File_output(lable_filename);
	if (File_output.is_open()) {
		gotoLine(File_output, position);
		std::string line;
		int counter = 0;

		while (std::getline(File_output, line) and counter < 1000) {
			std::istringstream iss(line);
			double a;
			int counter_2 = 0;
			while (iss >> a) {
				lable[counter][counter_2] = a;
				counter_2++;
			}
			counter++;
		}
		File_output.close();
	}
}

Rckg Generator::generate(void) {
	if (position % 1000 == 0 and position < filesize) {
		position++;
		get_data(input_filename, lable_filename);
		return Rckg{ vector<double>{input[position % 1000],input[position % 1000] + vectorinput_size},vector<double>{lable[position % 1000],lable[position % 1000] + vectoroutput_size} };
	}
	else {
		position++;
		return Rckg{ vector<double>{input[position % 1000],input[position % 1000] + vectorinput_size},vector<double>{lable[position % 1000],lable[position % 1000] + vectoroutput_size} };
	}
}