#ifndef Acivationfunction_H
#define Acivationfunction_H


#include <iostream>
#include <vector>
#include "Matrix.h"
#include "ANN.h"
using std::vector;
using std::pow;

double sigmoide(double zahl);
double del_sigmoide(double zahl);


double ReLU(double zahl);
double del_ReLU(double zahl);

#endif // !Acivationfunction_H
