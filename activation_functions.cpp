#include "activation_functions.h"


double sigmoide(double zahl) {
	return 1 / (1.0 + pow(2.7182818, (-zahl)));
}

double del_sigmoide(double zahl) {
	double eminusxp = pow(2.7182818, (-zahl));
	return eminusxp / ((1 + eminusxp) * (1 + eminusxp));
}


double ReLU(double zahl) {
	if (zahl < 0) {
		return 0.01 * zahl;
	}
	else {
		return zahl < 1 ? zahl : 0.99 + 0.01 * zahl; //m*x+b with m=1 and b = 0
	}
}

double del_ReLU(double zahl) {
	if (zahl < 0) {
		return 0.01;
	}
	else {
		return zahl < 1 ? 1 : 0.01;
	}
}