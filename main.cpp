#include <iostream>
#include <vector>
#include <stdlib.h>     /* srand, rand for initialising the ANN*/
#include <time.h>       /* time to create timseed*/
#include "Matrix.h"
#include "Generator.h"
#include "ANN.h"
#include "activation_functions.h"
using std::vector;
using std::cout;
using std::endl;

void print_out_learning_per_steps(vector<double> acuracy, int learning_steps) {
	for (std::size_t i = 0; i < acuracy.size(); i++) {
		cout << "Accuracy is: " << acuracy[i] << "after " << ((double)i + 1) * ((double)learning_steps) << " learning steps" << endl;
	}
}






int main(void) {
	//Load in Data and build a input-/label-geneartor
	Generator G("Data/images.dat", "Data/labels.dat", 784, 10);

	//Create or load a artificial neural net
	ANN A(vector<int> {784, 128, 16, 10}, sigmoide, del_sigmoide);
	//ANN A("MNISTtrained.dat", sigmoide, del_sigmoide); 

	//Test accuracy before training
	cout << "Acuracy is: " << A.Acuracy_test(G, 10000) << endl;

	//Train and test with a small amount of samples - print out the accuracy every 1000 trained samples
	int learning_steps = 1000;
	int number_of_train_samples = 3000;
	vector<double> acuracy = A.Checkout_learnrate(G, number_of_train_samples, learning_steps, 0.125);
	print_out_learning_per_steps(acuracy, learning_steps);
	bool b;
	std::cout << "Save? " << endl;
	std::cin >> b;
	if (b) {
		A.save("ANN_short_training");
	}

	return 0;
}